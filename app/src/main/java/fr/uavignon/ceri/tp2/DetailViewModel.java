package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> selectedBook;

    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSearchResult();
    }

    MutableLiveData<Book> getSelectedBook() { return selectedBook; }
    public void insertBook(Book newBook){ repository.insertBook(newBook);}
    public void getBook(long id){ repository.getBook(id);}
    public void deleteBook(long id){ repository.deleteBook(id);}
    public void updateBook(Book book){ repository.updateBook(book);}

}
