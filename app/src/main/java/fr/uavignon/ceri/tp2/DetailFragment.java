package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    DetailViewModel viewModel;

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Book book;
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        if (args.getBookNum() != -1) {
            viewModel.getBook(args.getBookNum());
            book = viewModel.getSelectedBook().getValue();
        }
        else {
            Button button = (Button) view.findViewById(R.id.buttonUpdate);
            book = new Book("","","","","");
            button.setText("Ajouter livre");
        }

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        observerSetup();


        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book.setAuthors(textAuthors.getText().toString());
                book.setTitle(textTitle.getText().toString());
                book.setGenres(textGenres.getText().toString());
                book.setPublisher(textPublisher.getText().toString());
                book.setYear(textYear.getText().toString());
                if (args.getBookNum() != -1) {
                    viewModel.updateBook(book);
                    Snackbar.make(view, "Livre mis à jour", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else {
                    if (textTitle.getText().toString().trim().length() > 1 && textAuthors.getText().toString().trim().length() > 1) {
                        viewModel.insertBook(book);
                        Snackbar.make(view,"Livre ajouté avec succés", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                                .navigate(R.id.action_SecondFragment_to_FirstFragment);
                    } else
                        Snackbar.make(view,"Il manque des informations", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                }
            }
        });
    }

    private void observerSetup() {
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(@Nullable final Book book) {
                        textTitle.setText(book.getTitle());
                        textAuthors.setText(book.getAuthors());
                        textYear.setText(book.getYear());
                        textGenres.setText(book.getGenres());
                        textPublisher.setText(book.getPublisher());
                    }
                });
    }
}